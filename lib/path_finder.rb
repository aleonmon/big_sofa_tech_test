#
# Shows the shortest path from a start node and end node of a given graph
#
class PathFinder
  attr_accessor :graph, :visited_nodes, :unvisited_nodes, :origin_node, :destination_node

  def initialize(origin_node:, destination_node:)
    self.graph            = set_graph
    self.unvisited_nodes  = graph.nodes.dup
    self.visited_nodes    = []
    self.origin_node      = set_origin_node(origin_node)
    self.destination_node = set_destination_node(destination_node)
  end

  def run
    calculate_shortest_paths
    show_shortest_path
  end

  private

  def calculate_shortest_paths
    until unvisited_nodes.empty?
      current_node = find_next_node

      unvisited_neighbour_nodes(current_node).each do |neighbour_node|
        edge = graph.find_edge(current_node, neighbour_node)
        distance_from_origin = current_node.distance_from_origin + edge.weight

        if distance_from_origin < neighbour_node.distance_from_origin
          neighbour_node.distance_from_origin = distance_from_origin
          neighbour_node.previous_node = current_node
        end
      end

      visited_nodes << current_node
      unvisited_nodes.delete(current_node)
    end
  end

  def unvisited_neighbour_nodes(node)
    graph.node_neighbours(node) - visited_nodes
  end

  # Find the node with the shortest known distance from the origin node
  def find_next_node
    graph.node_with_shortest_distance(scope: unvisited_nodes)
  end

  def show_shortest_path
    current_node = destination_node
    nodes = []

    until current_node.nil?
      nodes << current_node
      current_node = current_node.previous_node
    end

    puts "The shortest path from #{origin_node.name} to #{destination_node.name} is: " +
         nodes.reverse.map(&:name).join(", ")
  end

  def set_origin_node(origin_node)
    graph.nodes
      .find { |node| node.name == origin_node }
      .tap { |node| node.distance_from_origin = 0 }
  end

  def set_destination_node(destination_node)
    graph.nodes.find { |node| node.name == destination_node }
  end

  def set_graph
    node0 = Node.new(name: "0")
    node1 = Node.new(name: "1")
    node2 = Node.new(name: "2")
    node3 = Node.new(name: "3")
    node4 = Node.new(name: "4")
    node5 = Node.new(name: "5")
    node6 = Node.new(name: "6")
    node7 = Node.new(name: "7")

    graph = Graph.new
    graph.add_edge(Edge.new(start_node: node0, end_node: node1, weight: 10))
    graph.add_edge(Edge.new(start_node: node1, end_node: node2, weight: 4))
    graph.add_edge(Edge.new(start_node: node2, end_node: node3, weight: 15))
    graph.add_edge(Edge.new(start_node: node3, end_node: node7, weight: 9))
    graph.add_edge(Edge.new(start_node: node0, end_node: node4, weight: 15))
    graph.add_edge(Edge.new(start_node: node4, end_node: node5, weight: 8))
    graph.add_edge(Edge.new(start_node: node5, end_node: node7, weight: 10))
    graph.add_edge(Edge.new(start_node: node0, end_node: node6, weight: 21))
    graph.add_edge(Edge.new(start_node: node6, end_node: node7, weight: 7))

    graph
  end
end
