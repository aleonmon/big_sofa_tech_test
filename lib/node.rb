#
# This represents a vertex of a graph
#
class Node
  attr_accessor :name, :distance_from_origin, :previous_node

  def initialize(name:)
    self.name = name
    self.distance_from_origin = Float::INFINITY
    self.previous_node = nil
  end

  def ==(other)
    name == other.name
  end

  def to_s
    name
  end
end
