#
# This represents an edge of a graph
#
class Edge
  attr_accessor :start_node, :end_node, :weight

  def initialize(start_node:, end_node:, weight:)
    self.start_node = start_node
    self.end_node   = end_node
    self.weight     = weight
  end

  def ==(other)
    start_node == other.start_node && end_node == other.end_node
  end

  def to_s
    "#{start_node} -> #{end_node} [weight: #{weight}]"
  end
end
