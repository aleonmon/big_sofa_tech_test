#
# This represents a graph define by its vertex and edges
#
class Graph
  attr_reader :nodes, :edges

  def initialize
    @nodes = []
    @edges = []
  end

  def add_node(node)
    raise_duplicate_node_error(node) if node_exists?(node)

    nodes << node

    node
  end

  def add_edge(edge)
    raise_duplicate_edge_error(edge) if edge_exists?(edge)

    add_node(edge.start_node) unless node_exists?(edge.start_node)
    add_node(edge.end_node) unless node_exists?(edge.end_node)

    edges << edge

    edge
  end

  def node_neighbours(node)
    node_edges(node).map(&:end_node)
  end

  def find_edge(from_node, to_node)
    edges.find { |edge| edge.start_node == from_node && edge.end_node == to_node }
  end

  def node_with_shortest_distance(scope: nodes)
    nodes_list = nodes & scope
    shortest_distance = nodes_list.map(&:distance_from_origin).min
    nodes_list.find { |node| node.distance_from_origin == shortest_distance }
  end

  private

  def node_exists?(node)
    nodes.include?(node)
  end

  def edge_exists?(edge)
    edges.include?(edge)
  end

  def node_edges(node)
    edges.select { |edge| edge.start_node == node }
  end

  def raise_duplicate_node_error(node)
    raise "Error: This node already exits '#{node}'"
  end

  def raise_duplicate_edge_error(edge)
    raise "Error: This edge already exits #{edge}"
  end
end
