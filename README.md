# Big Sofa Tech Test
[![Codeship Status for aleonmon/big_sofa_tech_test](https://app.codeship.com/projects/228357c0-c9bd-0137-9ef1-5273def13563/status?branch=master)](https://app.codeship.com/projects/367794)
[![Coverage Status](https://coveralls.io/repos/bitbucket/aleonmon/big_sofa_tech_test/badge.svg?branch=master)](https://coveralls.io/bitbucket/aleonmon/big_sofa_tech_test?branch=master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/39c22588632547579e6d17e2b1a31eea)](https://www.codacy.com/manual/andres-leon-job/big_sofa_tech_test?utm_source=aleonmon@bitbucket.org&amp;utm_medium=referral&amp;utm_content=aleonmon/big_sofa_tech_test&amp;utm_campaign=Badge_Grade)

## Description

Represent this graph in Ruby and use Dijkstra's shortest path algorithm to show the fastest route from 0 to 7

```
digraph distances {
    rankdir=LR;
    size="8,5"
    node [shape = circle];
    0 -> 1 [ label = "10" ];
    1 -> 2 [ label = "4" ];
    2 -> 3 [ label = "15" ];
    3 -> 7 [ label = "9" ];
    0 -> 4 [ label = "15" ];
    4 -> 5 [ label = "8" ];
    5 -> 7 [ label = "10" ];
    0 -> 6 [ label = "21" ];
    6 -> 7 [ label = "7" ];
}

```
http://www.webgraphviz.com

## Approach
1. Apply OOP and TDD to represent a graph in Ruby
2. Use Dijkstra's shortest path algorithm
```
   Dijstrah's Algorithm

   Let distance of start vertex from start vertex = 0
   Let distance of all other vertices from start vertex = Infinity

   WHILE Vertices remain unvisited
     Visit unvisited vertex with smallest known distance from start vertex
     FOR each unvisited neighbour from start vertex
       Calculate the distance from start vertex
       If the calculated distance of this vertex is less than the known distance
         Update the shortest distnce to this vertex
         Update the previous vertex with the current vertex
       End If
     NEXT unvisited neighbour
     Add the current vertex to the list of visited vertices
   END WHILE

```
3. Show the fastest route from an origin and a destination
  Output sample:
```
    The shortest path from 0 to 7 is: 0, 6, 7

```

## Setting up

Make sure you are in the root path of the repository and run
```
$ bundle install
```

## Running Script

Make sure you are in the root path of the repository and run
```
$ bin/shortest_path
```

## Running Specs

Make sure you are in the root path of the repository and run
```
$ rspec spec
```

## Notes

**The Solution**

- The approach is intended to be as simple as possible
- Tried to respect SRP as much as I could
- Used development tools like Rubocop and Overcommit to ensure a good code style
- Used coverage tools to ensure that the code is fully tested


**To be improved**

- Clean Rubocop's TODO file
