require "spec_helper"

RSpec.describe Edge do
  subject { described_class.new(start_node: start_node, end_node: end_node, weight: weight) }
  let(:start_node) { Node.new(name: "A") }
  let(:end_node) { Node.new(name: "B") }
  let(:weight) { 10 }

  describe "#start_node" do
    it "has a start_node" do
      expect(subject.start_node).to eq(start_node)
    end
  end

  describe "#end_node" do
    it "has a end_node" do
      expect(subject.end_node).to eq(end_node)
    end
  end

  describe "#weight" do
    it "has a weight" do
      expect(subject.weight).to eq(weight)
    end
  end

  describe "#==" do
    let(:other_edge) { described_class.new(start_node: other_start_node, end_node: other_end_node, weight: 1) }

    context "when it is equal" do
      let(:other_start_node) { Node.new(name: "A") }
      let(:other_end_node) { Node.new(name: "B") }

      it { is_expected.to eq(other_edge) }
    end

    context "when it is different" do
      let(:other_start_node) { Node.new(name: "A") }
      let(:other_end_node) { Node.new(name: "C") }

      it { is_expected.to_not eq(other_edge) }
    end
  end

  describe "#to_s" do
    it "returns its start node and end node" do
      expect(subject.to_s).to eq("A -> B [weight: 10]")
    end
  end
end
