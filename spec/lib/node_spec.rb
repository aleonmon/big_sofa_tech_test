require "spec_helper"

RSpec.describe Node do
  subject { described_class.new(name: node_name) }

  describe "#name" do
    let(:node_name) { "A" }

    it "has a name" do
      expect(subject.name).to eq(node_name)
    end
  end

  describe "#==" do
    let(:node_name) { "A" }
    let(:other_node) { described_class.new(name: other_node_name) }

    context "when it is equal" do
      let(:other_node_name) { "A" }

      it { is_expected.to eq(other_node) }
    end

    context "when it is different" do
      let(:other_node_name) { "B" }

      it { is_expected.to_not eq(other_node) }
    end
  end

  describe "#to_s" do
    let(:node_name) { "A" }

    it "returns its name" do
      expect(subject.to_s).to eq(node_name)
    end
  end
end
