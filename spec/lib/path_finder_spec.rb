require "spec_helper"

RSpec.describe PathFinder do
  subject { described_class.new(origin_node: "0", destination_node: "7") }

  describe "#graph" do
    it "returns a predefined graph" do
      expect(subject.graph).to be_an_instance_of(Graph)
    end

    it "has the correct nodes" do
      expect(subject.graph.nodes.map(&:name).sort).to eq(%w[0 1 2 3 4 5 6 7])
    end

    it "has the correct edges" do
      edges = ["0 -> 1 [weight: 10]", "1 -> 2 [weight: 4]", "2 -> 3 [weight: 15]", "3 -> 7 [weight: 9]",
               "0 -> 4 [weight: 15]", "4 -> 5 [weight: 8]", "5 -> 7 [weight: 10]", "0 -> 6 [weight: 21]",
               "6 -> 7 [weight: 7]"]

      expect(subject.graph.edges.map(&:to_s)).to eq(edges)
    end
  end

  describe "#visited_nodes" do
    it "has the correct nodes" do
      expect(subject.visited_nodes.map(&:name)).to eq([])
    end
  end

  describe "#unvisited_nodes" do
    it "has the correct nodes" do
      expect(subject.unvisited_nodes.map(&:name).sort).to eq(%w[0 1 2 3 4 5 6 7])
    end
  end

  describe "#origin_node" do
    it "returns the correct node" do
      expect(subject.origin_node.name).to eq("0")
    end
  end

  describe "#destination_node" do
    it "returns the correct node" do
      expect(subject.destination_node.name).to eq("7")
    end
  end

  describe "#run" do
    it "calculates the shortest paths" do
      expect(subject).to receive(:show_shortest_path)

      subject.run

      dijkstra_info = subject.graph.nodes.map do |node|
        "node: #{node.name}, distance: #{node.distance_from_origin}, previous_node: #{node.previous_node&.name} "
      end.join("| ")

      expected_dijkstra_info = "node: 0, distance: 0, previous_node:  | node: 1, distance: 10, previous_node: 0 | " \
                               "node: 2, distance: 14, previous_node: 1 | node: 3, distance: 29, previous_node: 2 | " \
                               "node: 7, distance: 28, previous_node: 6 | node: 4, distance: 15, previous_node: 0 | " \
                               "node: 5, distance: 23, previous_node: 4 | node: 6, distance: 21, previous_node: 0 "

      expect(dijkstra_info).to eq(expected_dijkstra_info)
    end

    it "shows the shortest path from the origin node to the destination node" do
      expect { subject.run }.to output("The shortest path from 0 to 7 is: 0, 6, 7\n").to_stdout
    end
  end
end
