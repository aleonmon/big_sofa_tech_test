require "spec_helper"

RSpec.describe Graph do
  subject { described_class.new }

  describe "#nodes" do
    it "has an empty list of nodes" do
      expect(subject.nodes).to be_empty
    end
  end

  describe "#edges" do
    it "has an empty list of nodes" do
      expect(subject.edges).to be_empty
    end
  end

  describe "#add_node" do
    let(:node_1) { Node.new(name: "A") }

    before { subject.add_node(node_1) }

    context "with a valid node" do
      let(:node_2) { Node.new(name: "B") }

      before { subject.add_node(node_2) }

      it "adds a node successfully" do
        expect(subject.nodes).to eq([node_1, node_2])
      end
    end

    context "with an invalid node" do
      let(:node_2) { Node.new(name: "A") }

      it "raises an error" do
        expect { subject.add_node(node_2) }.to raise_error(nil, "Error: This node already exits 'A'")
      end
    end
  end

  describe "#add_edge" do
    let(:edge_1) { Edge.new(start_node: start_node_1, end_node: end_node_1, weight: 1) }
    let(:start_node_1) { Node.new(name: "A") }
    let(:end_node_1) { Node.new(name: "B") }

    before { subject.add_edge(edge_1) }

    context "with a valid edge" do
      let(:edge_2) { Edge.new(start_node: start_node_1, end_node: end_node_2, weight: 1) }
      let(:end_node_2) { Node.new(name: "C") }

      before { subject.add_edge(edge_2) }

      it "adds a edge successfully" do
        expect(subject.edges).to eq([edge_1, edge_2])
      end
    end

    context "with an invalid edge" do
      context "with a duplicate edge" do
        let(:edge_2) { Edge.new(start_node: start_node_1, end_node: end_node_1, weight: 1) }

        it "raises an error" do
          expect { subject.add_edge(edge_2) }.to raise_error(nil, "Error: This edge already exits A -> B [weight: 1]")
        end
      end
    end
  end

  # describe "#node_edges" do
  #   let(:edge_1) { Edge.new(start_node: node_1, end_node: node_2, weight: 1) }
  #   let(:edge_2) { Edge.new(start_node: node_1, end_node: node_3, weight: 2) }
  #   let(:edge_3) { Edge.new(start_node: node_1, end_node: node_4, weight: 3) }
  #   let(:edge_4) { Edge.new(start_node: node_2, end_node: node_4, weight: 4) }
  #   let(:node_1) { Node.new(name: "A") }
  #   let(:node_2) { Node.new(name: "B") }
  #   let(:node_3) { Node.new(name: "C") }
  #   let(:node_4) { Node.new(name: "D") }
  #
  #   before do
  #     subject.add_edge(edge_1)
  #     subject.add_edge(edge_2)
  #     subject.add_edge(edge_3)
  #   end
  #
  #   context "when node has edges" do
  #     it "returns a list of edges" do
  #       expect(subject.node_edges(node_1)).to eq([edge_1, edge_2, edge_3])
  #     end
  #   end
  #
  #   context "when node has no edges" do
  #     it "returns an empty list" do
  #       expect(subject.node_edges(node_3)).to eq([])
  #     end
  #   end
  # end

  describe "#node_neighbours" do
    let(:edge_1) { Edge.new(start_node: node_1, end_node: node_2, weight: 1) }
    let(:edge_2) { Edge.new(start_node: node_1, end_node: node_3, weight: 2) }
    let(:edge_3) { Edge.new(start_node: node_1, end_node: node_4, weight: 3) }
    let(:node_1) { Node.new(name: "A") }
    let(:node_2) { Node.new(name: "B") }
    let(:node_3) { Node.new(name: "C") }
    let(:node_4) { Node.new(name: "D") }

    before do
      subject.add_edge(edge_1)
      subject.add_edge(edge_2)
      subject.add_edge(edge_3)
    end

    context "when node has neighbours" do
      it "returns a list of node neighbours" do
        expect(subject.node_neighbours(node_1)).to eq([node_2, node_3, node_4])
      end
    end

    context "when node has no neighbours" do
      it "returns an empty list" do
        expect(subject.node_neighbours(node_4)).to eq([])
      end
    end
  end

  describe "#node_with_shortest_distance" do
    let(:edge_1) { Edge.new(start_node: node_1, end_node: node_2, weight: 1) }
    let(:edge_2) { Edge.new(start_node: node_1, end_node: node_3, weight: 2) }
    let(:edge_3) { Edge.new(start_node: node_1, end_node: node_4, weight: 3) }
    let(:edge_4) { Edge.new(start_node: node_2, end_node: node_4, weight: 4) }
    let(:node_1) { Node.new(name: "A") }
    let(:node_2) { Node.new(name: "B") }
    let(:node_3) { Node.new(name: "C") }
    let(:node_4) { Node.new(name: "D") }

    before do
      subject.add_edge(edge_1)
      subject.add_edge(edge_2)
      subject.add_edge(edge_3)

      node_1.distance_from_origin = 1
      node_2.distance_from_origin = 100
      node_3.distance_from_origin = 10
      node_4.distance_from_origin = 1000
    end

    it "returns the node with the shortest distance" do
      expect(subject.node_with_shortest_distance(scope: [node_2, node_3, node_4])).to eq(node_3)
    end
  end

  describe "#finde_edge" do
    let(:edge) { Edge.new(start_node: node_1, end_node: node_2, weight: 1) }
    let(:node_1) { Node.new(name: "A") }
    let(:node_2) { Node.new(name: "B") }

    before do
      subject.add_edge(edge)
    end

    context "when the edge exits" do
      it "returns the edge" do
        expect(subject.find_edge(node_1, node_2)).to eq(edge)
      end
    end

    context "when the edge does not exit" do
      let(:node_3) { Node.new(name: "C") }

      it "returns nil" do
        expect(subject.find_edge(node_1, node_3)).to eq(nil)
      end
    end
  end
end
