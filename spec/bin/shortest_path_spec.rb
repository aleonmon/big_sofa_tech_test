require "spec_helper"

RSpec.describe "bin/shortest_path" do
  describe "shortest_path execution" do
    it "executes successfully" do
      expect(`bin/shortest_path`).to eq("The shortest path from 0 to 7 is: 0, 6, 7\n")
    end
  end
end
